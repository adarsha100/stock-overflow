import flask
from flask import jsonify
import flask_sqlalchemy
import flask_restless
from flask_msearch import Search
from flask_marshmallow import Marshmallow
from flask_cors import CORS
from flask import request
from dotenv import load_dotenv
import os

#load environment variables
load_dotenv()
stage = os.getenv('stage', 'test')

#configure/create flask app
app = flask.Flask(__name__,
    static_folder = 'build/static',
    template_folder = 'build')
CORS(app)
app.config['IMG_FOLDER'] = 'build/img'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
if stage == 'production':
    app.config['DEBUG'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('RDS_DATABASE_URI')
else:
    app.config['DEBUG'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'


#set up frontend endpoints
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    return flask.render_template('index.html')

@app.route('/img/<path:filename>')
def serve_images(filename):
    return flask.send_from_directory(app.config['IMG_FOLDER'], filename)

#database configurations/definitions
db = flask_sqlalchemy.SQLAlchemy(app)
ma = Marshmallow(app)
search = Search(app, db)
search.create_index()

class StockIndex(db.Model):
    stock_ticker = db.Column(db.String, db.ForeignKey('stock.ticker'), primary_key=True)
    index_ticker = db.Column(db.String, db.ForeignKey('index.ticker'), primary_key=True)

class IndustryIndex(db.Model):
    industry_name = db.Column(db.String, db.ForeignKey('industry.name'), primary_key=True)
    index_ticker = db.Column(db.String, db.ForeignKey('index.ticker'), primary_key=True)

class Index(db.Model):
    __searchable__ = ['name', 'ticker']
    name = db.Column(db.String(100))
    ticker = db.Column(db.String(10), primary_key=True)
    current_price = db.Column(db.Float)
    monthly_movement = db.Column(db.Float)
    annual_movement = db.Column(db.Float)
    industries = db.relationship('IndustryIndex', backref='index')
    stocks = db.relationship('StockIndex', backref='index')

class IndexSchema(ma.ModelSchema):
    class Meta:
        model=Index

class Stock(db.Model):
    __searchable__ = ['name', 'ticker','industry_name']
    name = db.Column(db.String(100))
    ticker = db.Column(db.String(10), primary_key=True)
    indices = db.relationship('StockIndex', backref='stock')
    industry_name = db.Column(db.String, db.ForeignKey('industry.name'))
    current_price = db.Column(db.Float)
    volume = db.Column(db.Integer)
    website = db.Column(db.String(255))

class StockSchema(ma.ModelSchema):
    class Meta:
        model=Stock

class Industry(db.Model):
    __searchable__ = ['name', 'human_name']
    name = db.Column(db.String(100), primary_key=True)
    human_name = db.Column(db.String(100))
    stocks = db.relationship('Stock', backref='industry')
    annual_movement = db.Column(db.Float)
    monthly_movement = db.Column(db.Float)
    indices = db.relationship('IndustryIndex', backref='industry')

class IndustrySchema(ma.ModelSchema):
    class Meta:
        model=Industry

@app.route('/api/index/search')
def index_search():
    params = request.args.get('q').split(" ")
    total_out = []
    for p in params:
        indices = Index.query.msearch(p).all()
        index_schema = IndexSchema(many=True)
        output = index_schema.dump(indices)
        total_out.extend(output)
    return jsonify({'results':total_out})
    
@app.route('/api/stock/search')
def stock_search():
    params = request.args.get('q').split(" ")
    total_out = []
    for p in params:
        indices = Stock.query.msearch(p).all()
        index_schema = StockSchema(many=True)
        output = index_schema.dump(indices)
        total_out.extend(output)
    return jsonify({'results':total_out})

@app.route('/api/industry/search')
def industry_search():
    params = request.args.get('q').split(" ")
    total_out = []
    for p in params:
        indices = Industry.query.msearch(p).all()
        index_schema = IndustrySchema(many=True)
        output = index_schema.dump(indices)
        total_out.extend(output)
    return jsonify({'results':total_out})

@app.route('/api/search')
def all_search():
    params = request.args.get('q').split(" ")
    total_out = []
    for p in params:
        indices = Index.query.msearch(p).all()
        index_schema = IndexSchema(many=True)
        total_out.extend(index_schema.dump(indices))

        indices = Stock.query.msearch(p).all()
        index_schema = StockSchema(many=True)
        total_out.extend(index_schema.dump(indices))

        indices = Industry.query.msearch(p).all()
        index_schema = IndustrySchema(many=True)
        total_out.extend(index_schema.dump(indices))
    return jsonify({'results':total_out})

db.create_all()

# Create the Flask-Restless API manager.
manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)
# Create API endpoints, which will be available at /api/<tablename>
manager.create_api(Stock, methods=['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], results_per_page=0)
manager.create_api(Industry, methods=['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], results_per_page=0)
manager.create_api(Index, methods=['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], results_per_page=0)
manager.create_api(IndustryIndex, methods=['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], results_per_page=0)
manager.create_api(StockIndex, methods=['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], results_per_page=0)

# start the flask loop
app.run(host='0.0.0.0')


