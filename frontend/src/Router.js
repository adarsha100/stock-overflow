import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";

import Stocks from "./components/stock/Stocks";
import Industries from "./components/industry/Industries";
import Indices from "./components/index/Indices";

import StockPage from "./components/stock/StockPage";
import IndustryPage from "./components/industry/IndustryPage";
import IndexPage from "./components/index/IndexPage";

import Main from "./Main";
import About from "./About";
import Visual from "./Visual";

import Header from "./Header";

export default () => {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/" component={Main}></Route>
        <Route exact path="/about" component={About}></Route>

        <Route
          path="/stocks/:stock"
          render={props => <StockPage {...props} />}
        ></Route>
        <Route exact path="/stocks" component={Stocks}></Route>

        <Route
          path="/industries/:industry"
          render={props => <IndustryPage {...props} />}
        ></Route>
        <Route exact path="/industries" component={Industries}></Route>

        <Route
          path="/indices/:index"
          render={props => <IndexPage {...props} />}
        ></Route>
        <Route exact path="/indices" component={Indices}></Route>

        <Route exact path="/visualization" component={Visual}></Route>

        <Redirect from="*" to="/"></Redirect>
      </Switch>
    </Router>
  );
};
