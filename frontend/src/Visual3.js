import React, { Component } from "react";
import * as d3 from "d3";
import dow from "./csv/dow.text";

class Visual3 extends Component {
  componentDidMount() {
    var svg = d3.select(this.refs.svg),
      width = +svg.attr("width"),
      height = +svg.attr("height"),
      radius = Math.min(width, height) / 2,
      g = svg
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var randomColor = (function() {
      var golden_ratio_conjugate = 0.618033988749895;
      var h = Math.random();

      var hslToRgb = function(h, s, l) {
        var r, g, b;

        if (s == 0) {
          r = g = b = l; // achromatic
        } else {
          function hue2rgb(p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6) return p + (q - p) * 6 * t;
            if (t < 1 / 2) return q;
            if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
            return p;
          }

          var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
          var p = 2 * l - q;
          r = hue2rgb(p, q, h + 1 / 3);
          g = hue2rgb(p, q, h);
          b = hue2rgb(p, q, h - 1 / 3);
        }

        return (
          "#" +
          Math.round(r * 255).toString(16) +
          Math.round(g * 255).toString(16) +
          Math.round(b * 255).toString(16)
        );
      };

      return function() {
        h += golden_ratio_conjugate;
        h %= 1;
        return hslToRgb(h, 0.5, 0.6);
      };
    })();

    var pie = d3
      .pie()
      .sort(null)
      .value(function(d) {
        return d.number;
      });

    var path = d3
      .arc()
      .outerRadius(radius - 10)
      .innerRadius(0);

    var label = d3
      .arc()
      .outerRadius(radius - 40)
      .innerRadius(radius - 40);

    d3.json(dow)
      .then(data => {
        var arc = g
          .selectAll(".arc")
          .data(pie(data))
          .enter()
          .append("g")
          .attr("class", "arc");

        arc
          .append("path")
          .attr("d", path)
          .attr("fill", randomColor);

        arc
          .append("text")
          .attr("transform", function(d) {
            return "translate(" + label.centroid(d) + ")";
          })
          .attr("dy", "0.35em")
          .text(function(d) {
            return d.data.industry;
          });
      })
      .catch(err => console.log(err));
  }
  render() {
    return <svg ref="svg" width="1080" height="720"></svg>;
  }
}

export default Visual3;
