import React from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const ModelDisplay = () => {
  return (
    <div className="d-flex flex-row justify-content-center">
      <Card
        border="dark"
        style={{
          width: "18rem"
        }}
        className="ml-2 mr-2 text-center"
      >
        <Card.Header as="h5">Stock</Card.Header>
        <Card.Body className="mt-3">
          <Card.Text>
            <i className="fas fa-chart-line fa-5x"></i>
          </Card.Text>
          <Link to="/stocks">
            <Button id="stockButton" variant="outline-primary">
              Find Stock !
            </Button>
          </Link>
        </Card.Body>
      </Card>
      <Card
        border="dark"
        style={{
          width: "18rem"
        }}
        className="ml-2 mr-2 text-center"
      >
        <Card.Header as="h5">Industry</Card.Header>
        <Card.Body className="mt-3">
          <Card.Text>
            <i className="fas fa-industry fa-5x"></i>
          </Card.Text>
          <Link to="/industries">
            <Button id="industryButton" variant="outline-primary">
              Find Industry !
            </Button>
          </Link>
        </Card.Body>
      </Card>

      <Card
        border="dark"
        style={{
          width: "18rem"
        }}
        className="ml-2 mr-2 text-center"
      >
        <Card.Header as="h5">Index</Card.Header>
        <Card.Body>
          <Card.Text className="mt-3">
            <i className="fas fa-chart-pie fa-5x"></i>
          </Card.Text>
          <Link to="/indices">
            <Button id="indexButton" variant="outline-primary">
              Find Index !
            </Button>
          </Link>
        </Card.Body>
      </Card>
    </div>
  );
};

export default ModelDisplay;
