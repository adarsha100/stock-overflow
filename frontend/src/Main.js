import React, { Component } from "react";
import axios from "axios";
import {
  Button,
  Jumbotron,
  Container,
  InputGroup,
  FormControl,
  FormGroup,
  Table
} from "react-bootstrap";
import "./Main.css";
import ModelDisplay from "./ModelDisplay.js";
import { Link } from "react-router-dom";

import StocksTable from "./components/stock/StocksTable";
import IndustriesTable from "./components/industry/IndustriesTable";
import IndicesTable from "./components/index/IndicesTable";

class Main extends Component {
  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearchClick.bind(this);
    this.state = {
      splitwords: [],
      tempStocks: [],
      stocks: [],
      industries: [],
      indices: [],
      loading: false,
      searchValue: null,
      showResult: false,
      moreTen: false
    };
  }

  handleSearchClick = async e => {
    const { searchValue, moreTen, tempStocks } = this.state;
    this.setState({
      loading: true
    });
    if (searchValue !== null && searchValue !== "") {
      const temp = searchValue.split(" ");
      this.setState({
        showResult: true,
        splitwords: temp
      });
      const url1 = `https://www.stockoverflow.me/api/stock/search?q=${temp.join(
        "+"
      )}`;
      const url2 = `https://www.stockoverflow.me/api/industry/search?q=${temp.join(
        "+"
      )}`;
      const url3 = `https://www.stockoverflow.me/api/index/search?q=${temp.join(
        "+"
      )}`;
      const res1 = await axios.get(url1);
      const data = res1.data.results;
      const temp1 = [];

      if (data.length >= 10) {
        this.setState({
          moreTen: true
        });
      } else {
        this.setState({
          moreTen: false
        });
      }

      data.forEach(each => {
        temp1.push(each.ticker);
      });

      /* Not from stocks and should be from tempstocks because stocks can be modified */
      let newStocks = [];
      tempStocks.forEach(stock => {
        for (let i = 0; i < temp1.length; ++i) {
          if (temp1[i] === stock.ticker) {
            newStocks.push(stock);
          }
        }
      });

      if (moreTen) {
        this.setState({
          stocks: newStocks.slice(0, 10)
        });
      } else {
        this.setState({
          stocks: newStocks
        });
      }

      const res2 = await axios.get(url2);
      const industry_data = res2.data.results;
      const res3 = await axios.get(url3);
      const index_data = res3.data.results;
      this.setState({
        industries: industry_data,
        indices: index_data
      });
    }
    this.setState({
      loading: false
    });
  };

  async componentDidMount() {
    const res = await axios.get("https://www.stockoverflow.me/api/stock");
    const objects = res.data.objects.filter(
      object => object.industry != null && object.indices.length !== 0
    );
    /* Store entire stocks for later use */
    this.setState({
      tempStocks: objects
    });
  }

  render() {
    return (
      <div>
        <Jumbotron fluid>
          <Container id="mainText" className="text-center">
            <h1>StockOverFlow</h1>
            <FormGroup className="d-flex align-items-center flex-column">
              <InputGroup className="mb-3" style={{ width: "50%" }}>
                <FormControl
                  onChange={e => {
                    this.setState({ searchValue: e.target.value });
                  }}
                  placeholder="Explore stocks"
                />
                <InputGroup.Append>
                  <Button
                    onClick={this.handleSearchClick}
                    disabled={this.state.loading}
                  >
                    Submit
                  </Button>
                </InputGroup.Append>
              </InputGroup>
            </FormGroup>
          </Container>
        </Jumbotron>
        {!this.state.showResult ? (
          <ModelDisplay />
        ) : (
          <Container className="w-100" style={{ marginBottom: "50px" }}>
            {!this.state.loading && (
              <div>
                <p>
                  Please click Submit button again for loading if you don't see
                  a result. It may take some moment for fetching and showing{" "}
                </p>
                <div>
                  <h1> Stocks </h1>
                  <Table responsive striped bordered hover size="lg">
                    <StocksTable
                      stocks={this.state.stocks}
                      words={this.state.splitwords}
                    />
                  </Table>
                  {this.state.moreTen && (
                    <Link to="/stocks">
                      <div style={{ textAlign: "center" }}>
                        <Button> More... </Button>
                      </div>
                    </Link>
                  )}
                </div>
                <div>
                  <h1> Industries </h1>
                  <Table responsive striped bordered hover size="lg">
                    <IndustriesTable
                      industries={this.state.industries}
                      words={this.state.splitwords}
                    />
                  </Table>
                </div>
                <div>
                  <h1> Indices </h1>
                  <Table responsive striped bordered hover size="lg">
                    <IndicesTable
                      indices={this.state.indices}
                      words={this.state.splitwords}
                    />
                  </Table>
                </div>
              </div>
            )}
            {this.state.loading && <h2>loading...</h2>}
          </Container>
        )}
        ;
      </div>
    );
  }
}

export default Main;
