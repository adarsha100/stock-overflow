import React, {Component} from "react";
import * as d3 from "d3";
import axios from "axios";

export default class Visual5 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        };
    }

    drawChart() {
        let width = 962,
            height = 962;
        let pack = data => d3
            .pack()
            .size([
                width - 2,
                height - 2
            ])
            .padding(3)(d3.hierarchy({children: data}).sum(d => d.value))
        let color = d3.scaleOrdinal(this.state.data.map(d => d.energy), d3.schemeCategory10)
        let root = pack(this.state.data);

        let tooltip = d3
            .select("body")
            .append("div")
            .style("position", "absolute")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .style("color", "white")
            .style("padding", "8px")
            .style("background-color", "rgba(0, 0, 0, 0.75)")
            .style("border-radius", "6px")
            .style("font", "12px sans-serif")
            .text("tooltip");


        const svg = d3.select(this.refs.body)
            .append("svg")
            .attr("viewBox", [0, 0, width, height])
            .attr("font-size", 20)
            .attr("font-family", "sans-serif")
            .attr("text-anchor", "middle");

        const leaf = svg
            .selectAll("g")
            .data(root.leaves())
            .join("g")
            .attr("transform", d => `translate(${d.x + 1},${d.y + 1})`);

        leaf
            .append("circle")
            .attr("r", d => d.r)
            .attr("fill-opacity", 0.7)
            .attr("fill", d => color(d.data.energy))
            .on("mouseover", function (d) {
                tooltip.text(d.data.energy + ":\n" + d.value);
                tooltip.style("visibility", "visible");
            })
            .on("mousemove", function () {
                return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
            })
            .on("mouseout", function () {
                return tooltip.style("visibility", "hidden");
            });

        leaf
            .append("text")
            .selectAll("tspan")
            .data(d => d.data.energy.split(/(?=[A-Z][^A-Z])/g))
            .join("tspan")
            .attr("x", 0)
            .attr("y", (d, i, nodes) => `${i - nodes.length / 2 + 0.8}em`)
            .text(d => d);
    }

    fetchAndDraw = async() => {
        this.setState({isLoading: true});
        let url = "https://api.empoweringknowledge.me/api/states";
        let arr = [];
        for (let i = 1; i <= 6; i++) {
            let newUrl = url + '?page=' + i;
            let res = await axios.get(newUrl);
            arr.push(res.data.objects);
        }
        let con = [];
        let set = {};
        let k = 0;
        for (let i = 0; i < 6; i++) {
            var j = 0;
            while (j < arr[i].length) {
                let obj = arr[i][j];
                if (set[obj.primary_energy_source_1] === undefined) {
                    set[obj.primary_energy_source_1] = k;
                    k++;
                    let obj1 = {
                        energy: obj.primary_energy_source_1,
                        value: 0
                    };
                    con.push(obj1);
                }
                let key1 = set[obj.primary_energy_source_1];
                con[key1].value += 1;

                if (set[obj.primary_energy_source_2] === undefined) {
                    set[obj.primary_energy_source_2] = k;
                    k++;
                    let obj2 = {
                        energy: obj.primary_energy_source_2,
                        value: 0
                    }
                    con.push(obj2);
                }
                let key2 = set[obj.primary_energy_source_2];
                con[key2].value += 1;
                if (obj.primary_energy_source_3 != null) {
                    if (set[obj.primary_energy_source_3] === undefined) {
                        set[obj.primary_energy_source_3] = k;
                        k++;
                        let obj3 = {
                            energy: obj.primary_energy_source_3,
                            value: 0
                        }
                        con.push(obj3);
                    }
                    let key3 = set[obj.primary_energy_source_3];
                    con[key3].value += 1;
                }
                j++;
            }
        }
        this.setState({data: con});
        this.drawChart();
    }

    componentDidMount() {
        this.fetchAndDraw();
    }

    render() {
        return (
            <React.Fragment>
                <h2 className="text-center">
                    Number of States of Each Primary Energy Source</h2>
                <div className="justify-content-center" ref="body"></div>
            </React.Fragment>
        );
    }

}
