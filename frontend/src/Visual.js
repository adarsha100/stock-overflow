import React, { Component } from "react";
import { Tabs, Tab } from "react-bootstrap";
import Visual1 from "./Visual1";
import Visual2 from "./Visual2";
import Visual3 from "./Visual3";
import Visual5 from "./Visual5";
import Visual6 from "./Visual6";
import Visual7 from "./Visual7";
import "./Visual.css";

class Visual extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="title"> Visualizations </h1>
        <Tabs defaultActiveKey="index" id="uncontrolled-tab-example">
          <Tab eventKey="index" title="SP 500">
            <Visual1 />
          </Tab>
          <Tab eventKey="industry" title="NASDAQ">
            <Visual2 />
          </Tab>
          <Tab eventKey="stock" title="DOW JONES">
            <Visual3 />
          </Tab>
          <Tab eventKey="bills" title="Developer Visual #1">
            <Visual5/>
          </Tab>
          <Tab eventKey="energy" title="Developer Visual #2">
            <Visual6/>
          </Tab>
          <Tab eventKey="map" title="Developer Visual #3">
            <Visual7/>
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default Visual;
