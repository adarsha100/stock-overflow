/* eslint-disable default-case */
import React, { Component } from "react";
import { Card, CardDeck } from "react-bootstrap";
import "./About.css";

class About extends Component {
  constructor() {
    super();
    this.state = {
      totalCommits: 0,
      totalIssues: 0,
      totalUnitTests: 57,
      andrewCommitCount: 0,
      andrewIssueCount: 0,
      andrewUnitTest: 10,
      ickCommitCount: 0,
      ickIssueCount: 0,
      ickUnitTest: 10,
      adarshaCommitCount: 0,
      adarshaIssueCount: 0,
      adarshaUnitTest: 0,
      dingCommitCount: 0,
      dingIssueCount: 0,
      dingUnitTest: 15,
      simonCommitCount: 0,
      simonIssueCount: 0,
      simonUnitTest: 12
    };
  }

  getCounts() {
    let localCommit = {
      ickCommitCount: 0,
      andrewCommitCount: 0,
      adarshaCommitCount: 0,
      dingCommitCount: 0,
      simonCommitCount: 0,
      totalCommits: 0
    };

    for (let i = 1; i < 3; ++i) {
      const data =
        "https://gitlab.com/api/v4/projects/14500313/repository/commits?per_page=100&page=" +
        i;
      fetch(data)
        .then(response => response.json())
        .then(response => {
          console.log(response);
          response.forEach(commit => {
            ++localCommit.totalCommits;
            switch (commit.author_email) {
              case "ik0675@naver.com":
                ++localCommit.ickCommitCount;
                break;
              case "yangwenli@utexas.edu":
                ++localCommit.simonCommitCount;
                break;
              case "adarsha@cs.utexas.edu":
              case "adarsha.regmi@utexas.edu":
                ++localCommit.adarshaCommitCount;
                break;
              case "chang.andrew@utexas.edu":
              case "chang331006@gmail.com":
              case "achang.andrew@utexas.edu":
                ++localCommit.andrewCommitCount;
                break;
              case "dingz9926@utexas.edu":
                ++localCommit.dingCommitCount;
                break;
            }
          });
          this.setState({
            ...localCommit
          });
        });
    }

    let localIssue = {
      totalIssues: 0,
      andrewIssueCount: 0,
      ickIssueCount: 0,
      dingIssueCount: 0,
      simonIssueCount: 0,
      adarshaIssueCount: 0
    };

    const data2 =
      "https://gitlab.com/api/v4/projects/14500313/issues?per_page=100&page=1";
    fetch(data2)
      .then(response => response.json())
      .then(response => {
        response.forEach(issue => {
          ++localIssue.totalIssues;
          switch (issue.author.username) {
            case "ik0675":
              ++localIssue.ickIssueCount;
              break;
            case "yangwenli1":
              ++localIssue.simonIssueCount;
              break;
            case "adarsha100":
              ++localIssue.adarshaIssueCount;
              break;
            case "achang.andrew@utexas.edu":
              ++localIssue.andrewIssueCount;
              break;
            case "dingz9926":
              ++localIssue.dingIssueCount;
              break;
          }
          this.setState({
            ...localIssue
          });
        });
      });
  }

  componentDidMount() {
    this.getCounts();
  }

  render() {
    return (
      <div className="container" style={{ textAlign: "center" }}>
        <h2> Who We Are </h2>
        <div className="about">
          A group of dedicated engineers at the University of Texas at Austin
          who want to bring financial literacy to the masses
        </div>
        <h2> What is stockoverflow?</h2>
        <p>
          Stockoverflow is the website showing various stocks, industries, and
          indcies with their performance. The purpose of this site is that
          people are able to understand by seeing give information how the U.S.
          economy flows and how good each company manages. Also, it will help
          people make the best choices for investment. This website is for
          customers who are interested in share trading or understanding
          cooperations values.
        </p>
        <h2> Interesting result of Integrating disparate data </h2>
        <p>
        We integrate the data of different indices and their corresponding industries. 
        Overall, Pharmaceuticals Biotechnology and Technology Hardware Equipment are the industries that share the most proportions in three indices.
        </p>
        <h2> Creators </h2>
        <div className="d-flex flex-row justify-content-center">
          <Card style={{ width: "18rem" }} className="card img-fluid">
            <Card.Img variant="top" src="/img/namgung.png" />
            <Card.Body>
              <Card.Title>Ick Namgung</Card.Title>
              <Card.Text>
                <p>
                  Senior in Computer Science and like to explor new trends of
                  Science
                </p>
                <p> Front-End engineer </p>

                <span>
                  Commits: {this.state.ickCommitCount}
                  <br />
                  Issues: {this.state.ickIssueCount}
                  <br />
                  Unit Tests: {this.state.ickUnitTest}
                </span>
              </Card.Text>
            </Card.Body>
          </Card>
          <Card style={{ width: "18rem" }} className="card img-fluid">
            <Card.Img variant="top" src="/img/xie.png" />
            <Card.Body>
              <Card.Title>Jianjian Xie</Card.Title>
              <Card.Text>
                <p> Sophomore CS Student </p>
                <p> Front-End engineer </p>

                <span>
                  Commits: {this.state.simonCommitCount}
                  <br />
                  Issues: {this.state.simonIssueCount}
                  <br />
                  Unit Tests: {this.state.simonUnitTest}
                </span>
              </Card.Text>
            </Card.Body>
          </Card>
          ​
          <Card style={{ width: "18rem" }} className="card img-fluid">
            <Card.Img variant="top" src="/img/adarsha.png" />
            <Card.Body>
              <Card.Title>Adarsha Regmi</Card.Title>
              <Card.Text>
                <p> Sophomore CS Student </p>
                <p> Back-End engineer </p>

                <span>
                  Commits: {this.state.adarshaCommitCount}
                  <br />
                  Issues: {this.state.adarshaIssueCount}
                  <br />
                  Unit Tests: {this.state.adarshaUnitTest}
                </span>
              </Card.Text>
            </Card.Body>
          </Card>
          <Card style={{ width: "18rem" }} className="card img-fluid">
            <Card.Img variant="top" src="/img/ding.jpeg" />
            <Card.Body>
              <Card.Title>Alan Ding</Card.Title>
              <Card.Text>
                <p> Junior CS student </p>
                <p> Back-End engineer </p>

                <span>
                  Commits: {this.state.dingCommitCount}
                  <br />
                  Issues: {this.state.dingIssueCount}
                  <br />
                  Unit Tests: {this.state.dingUnitTest}
                </span>
              </Card.Text>
            </Card.Body>
          </Card>
          <Card style={{ width: "18rem" }} className="card img-fluid">
            <Card.Img variant="top" src="/img/chang.png" />
            <Card.Body>
              <Card.Title>Andrew M Chang</Card.Title>
              <Card.Text>
                <p> Sophomore CS Student </p>
                <p> Back-End engineer </p>
                <span>
                  Commits: {this.state.andrewCommitCount}
                  <br />
                  Issues: {this.state.andrewIssueCount}
                  <br />
                  Unit Tests: {this.state.andrewUnitTest}
                </span>
              </Card.Text>
            </Card.Body>
          </Card>
          <Card style={{ width: "18rem" }} className="card img-fluid">
            <Card.Img variant="top" src="" />
            <Card.Body>
              <Card.Title>Total</Card.Title>
              <Card.Text>
                <p>Commits / Issues / Unit Tests</p>
                <br />
                Commits: {this.state.totalCommits}
                <br />
                Issues: {this.state.totalIssues}
                <br />
                Unit Tests: {this.state.totalUnitTests}
              </Card.Text>
            </Card.Body>
          </Card>
        </div>

        <br></br>
        <br></br>
        <h2> Data for each DB Model</h2>
        <h3>Stock</h3>
        <p>
          <b>IEX API</b> was utilized to gather price and movement metrics for
          all the stocks.
          <b> Clearbit API</b> was utilized to gather company logos for each
          stock.
          <b> UniBit.ai API</b> was used for some other metrics, such as volume
          or market cap.
        </p>
        <h3>Index</h3>
        <p>
          All of the data for indices was gathered through <b>IEX Cloud API</b>,
          which provides a wider range of security types and instruments (ie.
          Indices).
          <b> UniBit.ai API</b> was used for some other metrics, such as volume
          or market cap.
        </p>
        <h3>Industry</h3>
        <p>
          The data for the industries was gathered through a combination of{" "}
          <b>IEX API</b> and <b>IEX Cloud API</b>.
        </p>
        <br></br>
        <br></br>

        <h2> Tools </h2>
        <CardDeck>
          <Card>
            <Card.Img variant="top" src="img/AWSCard.png" />
            <Card.Body>
              <Card.Title>AWS</Card.Title>
              <Card.Text>
                AWS is used to host our application, specifically through
                Elastic Beanstalk.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card>
            <Card.Img variant="top" src="img/FlaskCard.png" />
            <Card.Body>
              <Card.Title>Flask</Card.Title>
              <Card.Text>
                Flask is used as our application backend, handling and
                responding to API endpoints.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card>
            <Card.Img variant="top" src="img/SQLAlchemyCard.png" />
            <Card.Body>
              <Card.Title>SQLAlchemy</Card.Title>
              <Card.Text>
                SQLAlchemy is used to communicate with our DB easily through
                Pythonic means.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card>
            <Card.Img variant="top" src="img/PostmanCard.png" />
            <Card.Body>
              <Card.Title>Postman</Card.Title>
              <Card.Text>
                Postman is used to create and run test schemas for our API
                endpoint.
              </Card.Text>
            </Card.Body>
          </Card>
        </CardDeck>
        <CardDeck>
          <Card>
            <Card.Img variant="top" src="img/BootstrapCard.jpg" />
            <Card.Body>
              <Card.Title>Bootstrap</Card.Title>
              <Card.Text>
                Bootstrap is a CSS library that we used to style our application
                visually.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card>
            <Card.Img variant="top" src="img/ReactCard.png" />
            <Card.Body>
              <Card.Title>React</Card.Title>
              <Card.Text>
                React is a frontend framework that we used to create dynamic
                frontend pages.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card>
            <Card.Img variant="top" src="img/MochaCard.png" />
            <Card.Body>
              <Card.Title>Mocha</Card.Title>
              <Card.Text>
                Mocha is a tool used for unit testing our React frontend.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card>
            <Card.Img variant="top" src="img/SeleniumCard.jpg" />
            <Card.Body>
              <Card.Title>Selenium</Card.Title>
              <Card.Text>
                Selenium is used for automated end-to-end GUI testing.
              </Card.Text>
            </Card.Body>
          </Card>
        </CardDeck>

        <h2>
          <a id="gitrepo" href="https://gitlab.com/adarsha100/stock-overflow">
            GitLab repo
          </a>
        </h2>

        <h2>
          <a
            id="postman"
            href="https://documenter.getpostman.com/view/726128/SVtPYquQ?version=latest"
          >
            Postman API
          </a>
        </h2>
      </div>
    );
  }
}

export default About;
