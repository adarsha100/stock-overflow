import React from "react";
import { Link } from "react-router-dom";
import Highlighter from "react-highlight-words";

const StocksTable = ({ stocks, words }) => {
  return (
    <React.Fragment>
      <thead>
        <tr>
          <th id="attrName"> Name </th>
          <th id="attrTicket"> Ticker </th>
          <th id="attrPrice"> Current Price </th>
          <th id="attrIndex"> Index </th>
          <th id="attrSector"> Industry </th>
        </tr>
      </thead>
      <tbody>
        {stocks !== null &&
          stocks.map((stock, i) => {
            return (
              <tr key={i}>
                <td>
                  <Link to={`/stocks/${stock.ticker}`}>
                    <Highlighter
                      searchWords={words}
                      autoEscape={true}
                      textToHighlight={stock.name}
                    />
                  </Link>
                </td>
                <td>
                  {" "}
                  <Highlighter
                    searchWords={words}
                    autoEscape={true}
                    textToHighlight={stock.ticker}
                  />{" "}
                </td>
                <td> {stock.current_price} </td>
                <td>
                  {" "}
                  <Highlighter
                    searchWords={words}
                    autoEscape={true}
                    textToHighlight={stock.indices[0].index_ticker}
                  />{" "}
                </td>
                <td>
                  {" "}
                  <Highlighter
                    searchWords={words}
                    autoEscape={true}
                    textToHighlight={stock.industry.human_name}
                  />{" "}
                </td>
              </tr>
            );
          })}
      </tbody>
    </React.Fragment>
  );
};

export default StocksTable;
