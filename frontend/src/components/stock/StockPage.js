import React, { Component } from "react";
import { ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";

const linkUrl = "https://www.nasdaq.com/market-activity/stocks/";

export default class StockPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: linkUrl,
      logoSrc: null,
      website: null,
      stock: props.match.params.stock,
      ticker: null,
      sector: null,
      index: [],
      currentPrice: null
    };
  }

  componentDidMount() {
    const decodedUrl = `https://www.stockoverflow.me/api/stock/${this.state.stock}`;
    fetch(decodedUrl)
      .then(res => res.json())
      .then(data => {
        const indices = [];
        data.indices.forEach(index => {
          indices.push(index.index_ticker);
        });
        return this.setState({
          stock: data.name,
          ticker: data.ticker,
          sector: data.industry_name,
          index: indices,
          currentPrice: data.current_price,
          website: data.website,
          logoSrc: `https://logo.clearbit.com/${data.website}`
        });
      })
      .then(_ => {
        // state is filled now.
        return fetch(
          `https://www.stockoverflow.me/api/industry/${this.state.sector}`
        );
      })
      .then(res => res.json())
      .then(data => {
        this.setState({
          sector_human: data.human_name
        });
      })
      .catch(err => console.log(err));
  }

  render() {
    const link = this.state.index.map((index, i) => (
      <a
        key={i}
        id="link"
        href={`https://www.stockoverflow.me/indices/${index}`}
        className="mr-2"
      >
        {index}
      </a>
    ));
    return (
      <div style={{ backgroundColor: "#eee" }}>
        <div
          className="container d-flex justify-content-center align-items-center"
          style={{
            width: "50%",
            margin: "0 auto",
            height: "auto",
            minHeight: "100vh",
            backgroundColor: "white"
          }}
        >
          <ListGroup>
            <ListGroup.Item>
              <div className="d-flex justify-content-center">
                <img src={this.state.logoSrc} alt="company logo"></img>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <React.Fragment>
                <tbody className="instance-table">
                  <tr>
                    <td>Name</td>
                    <td>{this.state.stock}</td>
                  </tr>
                  <tr>
                    <td>Ticker</td>
                    <td>{this.state.ticker}</td>
                  </tr>
                  <tr>
                    <td>Industry</td>
                    <td>
                      <Link to={`/industries/${this.state.sector}`}>
                        {this.state.sector_human}
                      </Link>
                      <Link></Link>
                    </td>
                  </tr>
                  <tr>
                    <td>Index</td>
                    <td>{link}</td>
                  </tr>
                  <tr>
                    <td>Current Price</td>
                    <td>${this.state.currentPrice}</td>
                  </tr>
                  <tr>
                    <td>Chart</td>
                    <td>
                      <a id="link" href={this.state.url + this.state.ticker}>
                        {this.state.url}
                        {`${this.state.ticker}`}
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Company Site</td>
                    <td>
                      <a href={`${this.state.website}`}>{this.state.website}</a>
                    </td>
                  </tr>
                </tbody>
              </React.Fragment>
            </ListGroup.Item>
          </ListGroup>
        </div>
      </div>
    );
  }
}
