const checkboxes = [
  { name: "NameAscending", key: "na", label: "Name Ascending" },
  { name: "NameDescending", key: "nd", label: "Name Descending" },
  { name: "TickerAscending", key: "ta", label: "Ticker Ascending" },
  { name: "TickerDescending", key: "td", label: "Ticker Descending" },
  { name: "IndustryAscending", key: "ia", label: "Industry Ascending" },
  { name: "IndustryDescending", key: "id", label: "Industry Descending" },
  { name: "IndexAscending", key: "ixa", label: "Index Ascending" },
  { name: "IndexDescending", key: "ixd", label: "Index Descending" },
  { name: "PriceAscending", key: "pa", label: "Price Ascending" },
  { name: "PriceDescending", key: "pd", label: "Price Descending" }
];

export default checkboxes;
