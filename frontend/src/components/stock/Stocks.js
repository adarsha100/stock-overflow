import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Form,
  Jumbotron,
  FormGroup,
  InputGroup,
  Button,
  FormControl,
  Table,
  Container,
  Col
} from "react-bootstrap";
import StocksTable from "./StocksTable";
import MyPagination from "../Pagination";
import checkboxes from "./Checkboxes";
import Checkbox from "../Checkbox";

const Stocks = () => {
  const [stocks, setStocks] = useState([]);
  const [tempStocks, setTempstocks] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [stocksPerPage] = useState(10);
  const [checkedName, setCheckedName] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [minPrice, setMinPrice] = useState(-1);
  const [maxPrice, setMaxPrice] = useState(-1);
  const [splitwords, setSplitwords] = useState([]);

  const url = "https://www.stockoverflow.me/api/stock";

  useEffect(() => {
    const fetchStocks = async () => {
      setLoading(true);
      const res = await axios.get(url);
      const objects = res.data.objects.filter(
        object => object.industry != null && object.indices.length !== 0
      );
      setStocks(objects);
      setTempstocks(objects);
      setLoading(false);
    };
    fetchStocks();
  }, []);

  const indexOfLastStock = currentPage * stocksPerPage;
  const indexofFirstStock = indexOfLastStock - stocksPerPage;
  const currentStocks =
    stocks != null ? stocks.slice(indexofFirstStock, indexOfLastStock) : null;

  const paginate = pageNumber => setCurrentPage(pageNumber);

  const handleChange = async e => {
    setLoading(true);
    setCheckedName(e.target.name);
    let url =
      'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"name","direction":"asc"}]}';
    switch (e.target.name) {
      case "NameAscending":
        break;
      case "NameDescending":
        url =
          'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"name","direction":"desc"}]}';
        break;
      case "TickerAscending":
        url =
          'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"ticker","direction":"asc"}]}';
        break;
      case "TickerDescending":
        url =
          'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"ticker","direction":"desc"}]}';
        break;
      case "IndustryAscending":
        url =
          'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"industry_name","direction":"asc"}]}';
        break;
      case "IndustryDescending":
        url =
          'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"industry_name","direction":"desc"}]}';
        break;
      case "PriceAscending":
        url =
          'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"current_price","direction":"asc"}]}';
        break;
      case "PriceDescending":
        url =
          'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"current_price","direction":"desc"}]}';
        break;
      default:
        url =
          'https://www.stockoverflow.me/api/stock?q={"order_by":[{"field":"name","direction":"asc"}]}';
    }
    const res = await axios.get(url);
    const tempObjects = res.data.objects.filter(
      object => object.industry != null && object.indices.length !== 0
    );
    setStocks(tempObjects);
    setLoading(false);
  };

  const handleSearchClick = async e => {
    const tempValue = searchValue.split(" ");
    setSplitwords(tempValue);
    setLoading(true);
    const tempUrl = `https://www.stockoverflow.me/api/stock/search?q=${tempValue.join(
      "+"
    )}`;
    const res = await axios.get(tempUrl);
    const data = res.data.results;
    const temp = [];

    data.forEach(each => {
      temp.push(each.ticker);
    });

    let newStocks = [];
    tempStocks.forEach(stock => {
      const ticker = stock.ticker;
      for (let i = 0; i < temp.length; ++i) {
        if (temp[i] === ticker) {
          newStocks.push(stock);
        }
      }
    });
    setStocks(newStocks);
    setLoading(false);
  };

  const handleMinPrice = e => {
    setMinPrice(e.target.value);
  };

  const handleMaxPrice = e => {
    setMaxPrice(e.target.value);
  };

  const handleFilterClick = async () => {
    setLoading(true);
    let min = minPrice;
    let max = maxPrice;
    if (min === "") min = -1;
    if (max === "") max = -1;
    let tempUrl = "https://www.stockoverflow.me/api/stock";
    if (min >= 0) {
      if (max >= 0) {
        tempUrl = `https://www.stockoverflow.me/api/stock?q={"filters":[{"name":"current_price","op":"ge","val":${min}},{"name":"current_price","op":"le","val":${max}}]}`;
      } else {
        tempUrl = `https://www.stockoverflow.me/api/stock?q={"filters":[{"name":"current_price","op":"ge","val":${min}}]}`;
      }
    } else {
      if (max >= 0) {
        tempUrl = `https://www.stockoverflow.me/api/stock?q={"filters":[{"name":"current_price","op":"le","val":${max}}]}`;
      } else {
        tempUrl = "https://www.stockoverflow.me/api/stock";
      }
    }
    const res = await axios.get(tempUrl);
    const tempObjects = res.data.objects.filter(
      object => object.industry != null && object.indices.length !== 0
    );
    setStocks(tempObjects);
    setLoading(false);
  };

  return (
    <div>
      <Jumbotron fluid>
        <FormGroup className="d-flex align-items-center flex-column">
          <InputGroup className="mb-3" style={{ width: "50%" }}>
            <FormControl
              onChange={e => setSearchValue(e.target.value)}
              placeholder="Search Stock"
            />
            <InputGroup.Append>
              <Button onClick={handleSearchClick} variant="outline-secondary">
                Submit
              </Button>
            </InputGroup.Append>
          </InputGroup>
          <Form.Group>
            {checkboxes.map(item => (
              <Form.Check key={item.key}>
                <Checkbox
                  name={item.name}
                  checked={checkedName === item.name}
                  onChange={handleChange}
                ></Checkbox>
                {item.name}
              </Form.Check>
            ))}
          </Form.Group>

          <Form>
            <Form.Row>
              <span style={{ color: "white", paddingTop: "13px" }}>
                Current Price
              </span>
              <Col>
                <Form.Control
                  type="number"
                  onChange={handleMinPrice}
                  placeholder="Min. Price"
                />
              </Col>
              <Col>
                <Form.Control
                  type="number"
                  onChange={handleMaxPrice}
                  placeholder="Max. Price"
                />
              </Col>
            </Form.Row>
            <Button onClick={handleFilterClick} variant="secondary">
              Apply
            </Button>
          </Form>
        </FormGroup>
      </Jumbotron>
      <Container className="w-100" style={{ marginBottom: "50px" }}>
        {!loading && (
          <Table responsive striped bordered hover size="lg">
            <StocksTable stocks={currentStocks} words={splitwords} />
          </Table>
        )}
        {loading && <h2>loading...</h2>}
        <MyPagination
          itemsPerPage={stocksPerPage}
          totalItems={stocks != null ? stocks.length : 0}
          paginate={paginate}
          currentPage={currentPage}
        />
      </Container>
    </div>
  );
};

export default Stocks;
