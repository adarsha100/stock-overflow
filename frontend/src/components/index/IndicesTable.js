import React from "react";
import { Link } from "react-router-dom";
import Highlighter from "react-highlight-words";

const IndicesTable = ({ indices, words }) => {
  return (
    <React.Fragment>
      <thead className="tableHead">
        <tr>
          <th> Name </th>
          <th> Current Value </th>
          <th> Number of Stocks </th>
          <th> Monthly Movement </th>
          <th> Annual Movement </th>
        </tr>
      </thead>
      <tbody>
        {indices !== null &&
          indices.map((index, i) => {
            return (
              <tr key={i}>
                <td>
                  <Link to={`/indices/${index.ticker}`}>
                    <Highlighter
                      searchWords={words}
                      autoEscape={true}
                      textToHighlight={index.name}
                    />
                  </Link>
                </td>
                <td> {index.current_price} </td>
                <td> {index.stocks.length} </td>
                <td> {(index.monthly_movement * 100).toFixed(2)}% </td>
                <td> {(index.annual_movement * 100).toFixed(2)}% </td>
              </tr>
            );
          })}
      </tbody>
    </React.Fragment>
  );
};

export default IndicesTable;
