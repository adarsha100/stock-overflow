import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Form,
  Jumbotron,
  FormGroup,
  InputGroup,
  Button,
  FormControl,
  Table,
  Container,
  Col
} from "react-bootstrap";
import IndicesTable from "./IndicesTable";
import MyPagination from "../Pagination";
import checkboxes from "./Checkboxes";
import Checkbox from "../Checkbox";

const Indices = () => {
  const [indices, setIndices] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [indicesPerPage] = useState(10);
  const [checkedName, setCheckedName] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [minValue, setMinValue] = useState(-1);
  const [maxValue, setMaxValue] = useState(-1);
  const [splitwords, setSplitwords] = useState([]);

  const url = "https://www.stockoverflow.me/api/index";

  useEffect(() => {
    const fetchIndices = async () => {
      setLoading(true);
      const res = await axios.get(url);
      const objects = res.data.objects;
      setIndices(objects);
      setLoading(false);
    };
    fetchIndices();
  }, []);

  const indexOfLastIndices = currentPage * indicesPerPage;
  const indexofFirstIndex = indexOfLastIndices - indicesPerPage;
  const currentIndices =
    indices != null
      ? indices.slice(indexofFirstIndex, indexOfLastIndices)
      : null;

  const paginate = pageNumber => setCurrentPage(pageNumber);

  const handleChange = async e => {
    setLoading(true);
    setCheckedName(e.target.name);
    let url =
      'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"name","direction":"asc"}]}';
    switch (e.target.name) {
      case "NameAscending":
        break;
      case "NameDescending":
        url =
          'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"name","direction":"desc"}]}';
        break;
      case "ValueAscending":
        url =
          'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"current_price","direction":"asc"}]}';
        break;
      case "ValueDescending":
        url =
          'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"current_price","direction":"desc"}]}';
        break;
      case "MonthlyAscending":
        url =
          'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"monthly_movement","direction":"asc"}]}';
        break;
      case "MonthlyDescending":
        url =
          'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"monthly_movement","direction":"desc"}]}';
        break;
      case "AnnualAscending":
        url =
          'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"annual_movement","direction":"asc"}]}';
        break;
      case "AnnualDescending":
        url =
          'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"annual_movement","direction":"desc"}]}';
        break;
      default:
        url =
          'https://www.stockoverflow.me/api/index?q={"order_by":[{"field":"name","direction":"asc"}]}';
    }
    const res = await axios.get(url);
    setIndices(res.data.objects);
    setLoading(false);
  };

  const handleSearchClick = async e => {
    const tempSplit = searchValue.split(" ");
    setSplitwords(tempSplit);
    setLoading(true);
    const tempUrl = `https://www.stockoverflow.me/api/index/search?q=${tempSplit.join(
      "+"
    )}`;
    console.log(tempUrl);
    const res = await axios.get(tempUrl);
    console.log(res);
    setIndices(res.data.results);
    setLoading(false);
  };

  const handleMinChange = e => {
    setMinValue(e.target.value);
  };

  const handleMaxChange = e => {
    setMaxValue(e.target.value);
  };

  const handleFilterClick = async () => {
    setLoading(true);
    let min = minValue;
    let max = maxValue;
    if (min === "") min = -1;
    if (max === "") max = -1;
    let tempUrl = "https://www.stockoverflow.me/api/index";
    if (min >= 0) {
      if (max >= 0) {
        tempUrl = `https://www.stockoverflow.me/api/index?q={"filters":[{"name":"current_price","op":"ge","val":${min}},{"name":"current_price","op":"le","val":${max}}]}`;
      } else {
        tempUrl = `https://www.stockoverflow.me/api/index?q={"filters":[{"name":"current_price","op":"ge","val":${min}}]}`;
      }
    } else {
      if (max >= 0) {
        tempUrl = `https://www.stockoverflow.me/api/index?q={"filters":[{"name":"current_price","op":"le","val":${max}}]}`;
      } else {
        tempUrl = "https://www.stockoverflow.me/api/index";
      }
    }
    const res = await axios.get(tempUrl);
    setIndices(res.data.objects);
    setLoading(false);
  };

  return (
    <div>
      <Jumbotron fluid>
        <FormGroup className="d-flex align-items-center flex-column">
          <InputGroup className="mb-3" style={{ width: "50%" }}>
            <FormControl
              onChange={e => setSearchValue(e.target.value)}
              placeholder="Search Index"
            />
            <InputGroup.Append>
              <Button onClick={handleSearchClick} variant="outline-secondary">
                Submit
              </Button>
            </InputGroup.Append>
          </InputGroup>
          <Form.Group>
            {checkboxes.map((item, i) => (
              <Form.Check key={i}>
                <Checkbox
                  name={item.name}
                  checked={checkedName === item.name}
                  onChange={handleChange}
                ></Checkbox>
                {item.name}
              </Form.Check>
            ))}
          </Form.Group>

          <Form>
            <Form.Row>
              <span style={{ color: "white", paddingTop: "13px" }}>
                Current Value{" "}
              </span>
              <Col>
                <Form.Control
                  type="number"
                  onChange={handleMinChange}
                  placeholder="Min. Value"
                />
              </Col>
              <Col>
                <Form.Control
                  type="number"
                  onChange={handleMaxChange}
                  placeholder="Max. Value"
                />
              </Col>
            </Form.Row>
            <Button onClick={handleFilterClick} variant="secondary">
              Apply
            </Button>
          </Form>
        </FormGroup>
      </Jumbotron>
      <Container className="w-100" style={{ marginBottom: "50px" }}>
        {!loading && (
          <Table responsive striped bordered hover size="lg">
            <IndicesTable indices={currentIndices} words={splitwords} />
          </Table>
        )}
        {loading && <h2>loading...</h2>}
        <MyPagination
          itemsPerPage={indicesPerPage}
          totalItems={indices != null ? indices.length : 0}
          paginate={paginate}
          currentPage={currentPage}
        />
      </Container>
    </div>
  );
};
export default Indices;
