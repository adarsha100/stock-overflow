const checkboxes = [
  { name: "NameAscending", key: "na", label: "Name Ascending" },
  { name: "NameDescending", key: "nd", label: "Name Descending" },
  { name: "ValueAscending", key: "va", label: "Current Value Ascending" },
  { name: "ValueDescending", key: "vd", label: "Current Value Descending" },
  { name: "MonthlyAscending", key: "ma", label: "Monthly Ascending" },
  { name: "MonthlyDescending", key: "md", label: "Monthly Descending" },
  { name: "AnnualAscending", key: "aa", label: "Annual Ascending" },
  { name: "AnnualDescending", key: "ad", label: "Annual Descending" }
];

export default checkboxes;
