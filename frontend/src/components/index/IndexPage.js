import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ListGroup, Accordion, Card, Button } from "react-bootstrap";

/* ({match: { params: {stock}}} */
export default class IndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: props.match.params.index,
      industry: [],
      ticker: "",
      sector: "",
      currentPrice: 0,
      month: 0,
      annual: 0,
      name: "",
      stocks: []
    };
  }

  componentDidMount() {
    const decodedUrl = `https://www.stockoverflow.me/api/index/${this.state.index}`;
    fetch(decodedUrl)
      .then(res => res.json())
      .then(data => {
        const industries = [];
        data.industries.forEach(industry => {
          industries.push(industry.industry_name);
        });
        const stock_list = [];
        data.stocks.forEach(stock => {
          stock_list.push(stock.stock_ticker);
        });
        this.setState({
          name: data.name,
          industry: industries,
          numStocks: data.stocks.length,
          month: data.monthly_movement,
          annual: data.annual_movement,
          ticker: data.ticker,
          currentPrice: data.current_price,
          stocks: stock_list
        });
      })
      .catch(err => console.log(err));
  }

  render() {
    const toHumanName = {
      energy: "Energy",
      foodstaplesretailing: "Food Staples Retail",
      pharmaceuticalsbiotechnology: "Pharmaceutical Biotechnology",
      telecommunicationservices: "Telecommunication Services",
      technologyhardwareequipment: "Technology Hardware Equipment",
      householdpersonalproducts: "Household Personal Products",
      materials: "Materials",
      diversifiedfinancials: "Diversified Financials",
      utilities: "Utilities"
    };
    const link = this.state.industry.map((ind, i) => (
      <li>
        <Link key={i} to={`/industries/${ind}`} className="mr-2">
          {toHumanName[ind]}
        </Link>
      </li>
    ));
    const stocks = this.state.stocks.map((st, i) => (
      <li>
        <Link key={i} to={`/stocks/${st}`} className="mr-2">
          {st}
        </Link>
      </li>
    ));

    return (
      <div style={{ backgroundColor: "#eee" }}>
        <div
          className="container d-flex justify-content-center"
          style={{
            width: "100%",
            margin: "0 auto",
            backgroundColor: "white",
            height: "auto",
            minHeight: "100vh"
          }}
        >
          <ListGroup style={{marginTop: "10px", width: "50vw"}}>
            <ListGroup.Item>
              <div>
                <tbody class="instance-table-index">
                  <tr>
                    <td>Name</td>
                    <td>{this.state.name}</td>
                  </tr>
                  <tr>
                    <td>Ticker</td>
                    <td>{this.state.ticker}</td>
                  </tr>
                  <tr>
                    <td>Current Value</td>
                    <td>${this.state.currentPrice}</td>
                  </tr>
                  <tr>
                    <td>Monthly Movement</td>
                    <td>{this.state.month.toFixed(2)}%</td>
                  </tr>
                  <tr>
                    <td>Annual Movement</td>
                    <td>{this.state.annual.toFixed(2)}%</td>
                  </tr>
                </tbody>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <Accordion>
                <Card>
                  <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                      Industries
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body><ul>{link}</ul></Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey="1">
                      Stocks
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="1">
                    <Card.Body><ul>{stocks}</ul></Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            </ListGroup.Item>
          </ListGroup>
        </div>
      </div>
    );
  }
}
