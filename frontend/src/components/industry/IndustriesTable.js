import React from "react";
import { Link } from "react-router-dom";
import Highlighter from "react-highlight-words";

const IndustriesTable = ({ industries, words }) => {
  return (
    <React.Fragment>
      <thead>
        <tr>
          <th> Name </th>
          <th> Number of stocks </th>
          <th> Monthly Movement </th>
          <th> Annual Movement </th>
          <th> Index </th>
        </tr>
      </thead>
      <tbody>
        {industries !== null &&
          industries.map((industry, i) => {
            return (
              <tr key={i}>
                <td>
                  <Link to={`/industries/${industry.name}`}>
                    <Highlighter
                      searchWords={words}
                      autoEscape={true}
                      textToHighlight={industry.human_name}
                    />
                  </Link>
                </td>
                <td> {industry.stocks.length} </td>
                <td> {(industry.monthly_movement * 100).toFixed(2)}% </td>
                <td> {(industry.annual_movement * 100).toFixed(2)}% </td>
                <td>
                  {" "}
                  <Highlighter
                    searchWords={words}
                    autoEscape={true}
                    textToHighlight={industry.indices[0].index_ticker}
                  />{" "}
                </td>
              </tr>
            );
          })}
      </tbody>
    </React.Fragment>
  );
};

export default IndustriesTable;
