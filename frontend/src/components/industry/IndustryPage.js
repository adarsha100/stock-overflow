import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ListGroup, Accordion, Card, Button } from "react-bootstrap";

const linkUrl = "https://www.nasdaq.com/market-activity/stocks/";

export default class IndustryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: linkUrl,
      industry: props.match.params.industry,
      images: {
        diversifiedfinancials: "financial.jpg",
        energy: "oil.jpg",
        foodstaplesretailing: "shopping.jpg",
        householdpersonalproducts: "household.jpeg",
        materials: "material.jpeg",
        pharmaceuticalsbiotechnology: "health.jpg",
        technologyhardwareequipment: "electronictechnology.jpg",
        telecommunicationservices: "tele.jpg",
        utilities: "utilities.jpg"
      },
      numStocks: 0,
      month: 0,
      annual: 0,
      index: null,
      humanName: null,
      allStocks: []
    };
  }

  componentDidMount() {
    const decodedUrl = `https://www.stockoverflow.me/api/industry/${this.state.industry}`;
    fetch(decodedUrl)
      .then(res => res.json())
      .then(data => {
        const stock_list = [];
        data.stocks.forEach(stock => {
          stock_list.push(stock.ticker);
        });
        this.setState({
          industry: data.name,
          numStocks: data.stocks.length,
          month: data.monthly_movement,
          annual: data.annual_movement,
          index: data.indices[0].index_ticker,
          humanName: data.human_name,
          allStocks: stock_list
        });
      })
      .catch(err => console.log(err));
  }

  render() {
    const stocks = this.state.allStocks.map((st, i) => (
      <li>
        <Link key={i} to={`/stocks/${st}`} className="mr-2">
          {st}
        </Link>
      </li>
    ));
    return (
      <div style={{ backgroundColor: "#eee" }}>
        <div
          className="container d-flex justify-content-center"
          style={{
            width: "100%",
            margin: "0 auto",
            backgroundColor: "white",
            height: "auto",
            minHeight: "100vh"
          }}
        >
          <ListGroup style={{marginTop: "10px", width: "50vw"}}>
            <ListGroup.Item>
              <div class="d-flex justify-content-center">
                <img
                  src={`/img/${this.state.images[this.state.industry]}`}
                  alt={`${this.state.humanName} industry`}
                  style={{ width: 250 }}
                />
              </div>

            </ListGroup.Item>
            <ListGroup.Item>
              <div>
                <tbody class="instance-table-index">
                  <tr>
                    <td>Name</td>
                    <td>{this.state.humanName}</td>
                  </tr>
                  <tr>
                    <td>Number of Stocks</td>
                    <td>{this.state.numStocks} Stocks</td>
                  </tr>
                  <tr>
                    <td>Monthly Movement</td>
                    <td>{this.state.month.toFixed(2)}%</td>
                  </tr>
                  <tr>
                    <td>Annual Movement</td>
                    <td>{this.state.annual.toFixed(2)}%</td>
                  </tr>
                </tbody>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <Accordion>
                <Card>
                  <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                      Indices
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>
                      <Link
                        to={`/indices/${this.state.index}`}
                      >
                        {this.state.index}
                      </Link>
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey="1">
                      Stocks
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="1">
                    <Card.Body><ul>{stocks}</ul></Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            </ListGroup.Item>
          </ListGroup>
        </div>
      </div>
    );
  }
}
