import React from "react";
import { Card, CardDeck } from "react-bootstrap";
import Highlighter from "react-highlight-words";

const images = {
  "Diversified Financials": "financial.jpg",
  Energy: "oil.jpg",
  "Food Staples Retail": "shopping.jpg",
  "Household/Personal Products": "household.jpeg",
  Materials: "material.jpeg",
  "Pharmaceuticals Biotechnology": "health.jpg",
  "Technology Hardware Equipment": "electronictechnology.jpg",
  "Telecommunication Services": "tele.jpg",
  Utilities: "utilities.jpg"
};

const IndustriesCards = ({ industries, words }) => {
  return (
    <div className="container">
      <div className="d-flex flex-row justify-content-center">
        <CardDeck>
          {industries !== null &&
            industries.map((industry, i) => {
              const index_tickers = industry.indices.map((indices, j) => {
                return (
                  <li className="text-left" key={j}>
                    <Highlighter
                      searchWords={words}
                      autoEscape={true}
                      textToHighlight={indices.index_ticker}
                    />
                  </li>
                );
              });
              return (
                <Card
                  key={i}
                  style={{
                    width: "40rem"
                  }}
                >
                  <Card.Img
                    top="true"
                    width="100%"
                    variant="top"
                    src={`/img/${images[industry.human_name]}`}
                  />
                  <Card.Body>
                    <Card.Title>
                      <Highlighter
                        searchWords={words}
                        autoEscape={true}
                        textToHighlight={industry.human_name}
                      />
                    </Card.Title>
                    <Card.Text>
                      <span>
                        Number of Stocks: {industry.stocks.length}
                        <br />
                        Monthly Movement:{" "}
                        {(industry.monthly_movement * 100).toFixed(2)}%
                        <br />
                        Annual Movement:{" "}
                        {(industry.annual_movement * 100).toFixed(2)}%
                        <br />
                        Index: {index_tickers}
                      </span>
                    </Card.Text>
                    <Card.Link href={`/industries/${industry.name}`}>
                      <span>More Details</span>
                    </Card.Link>
                  </Card.Body>
                </Card>
              );
            })}
        </CardDeck>
      </div>
    </div>
  );
};

export default IndustriesCards;
