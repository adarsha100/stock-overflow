import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Form,
  Jumbotron,
  FormGroup,
  InputGroup,
  Button,
  FormControl,
  Container
} from "react-bootstrap";
import MyPagination from "../Pagination";
import checkboxes from "./Checkboxes";
import Checkbox from "../Checkbox";
import IndustriesCards from "./IndustriesCards";

const Industries = () => {
  const [industries, setIndustries] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [industriesPerPage] = useState(5);
  const [checkedName, setCheckedName] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [typeValue, setTypeValue] = useState("");
  const [splitwords, setSplitwords] = useState([]);

  const url = "https://www.stockoverflow.me/api/industry";

  useEffect(() => {
    const fetchIndustries = async () => {
      setLoading(true);
      const res = await axios.get(url);
      const objects = res.data.objects;
      setIndustries(objects);
      setLoading(false);
    };
    fetchIndustries();
  }, []);

  const indexOfLastIndustry = currentPage * industriesPerPage;
  const indexofFirstIndustry = indexOfLastIndustry - industriesPerPage;
  const currentIndustries =
    industries != null
      ? industries.slice(indexofFirstIndustry, indexOfLastIndustry)
      : null;

  const paginate = pageNumber => setCurrentPage(pageNumber);

  const handleChange = async e => {
    setLoading(true);
    setCheckedName(e.target.name);
    let url =
      'https://www.stockoverflow.me/api/industry?q={"order_by":[{"field":"name","direction":"asc"}]}';
    switch (e.target.name) {
      case "NameAscending":
        break;
      case "NameDescending":
        url =
          'https://www.stockoverflow.me/api/industry?q={"order_by":[{"field":"name","direction":"desc"}]}';
        break;
      case "MonthlyAscending":
        url =
          'https://www.stockoverflow.me/api/industry?q={"order_by":[{"field":"monthly_movement","direction":"asc"}]}';
        break;
      case "MonthlyDescending":
        url =
          'https://www.stockoverflow.me/api/industry?q={"order_by":[{"field":"monthly_movement","direction":"desc"}]}';
        break;
      case "AnnualAscending":
        url =
          'https://www.stockoverflow.me/api/industry?q={"order_by":[{"field":"annual_movement","direction":"asc"}]}';
        break;
      case "AnnualDescending":
        url =
          'https://www.stockoverflow.me/api/industry?q={"order_by":[{"field":"annual_movement","direction":"desc"}]}';
        break;
      default:
        url =
          'https://www.stockoverflow.me/api/industry?q={"order_by":[{"field":"name","direction":"asc"}]}';
    }

    const res = await axios.get(url);
    setIndustries(res.data.objects);
    setLoading(false);
  };

  const handleSearchClick = async e => {
    const tempValue = searchValue.split(" ");
    setSplitwords(tempValue);
    setLoading(true);
    let tempUrl = `https://www.stockoverflow.me/api/industry/search?q=${tempValue.join(
      "+"
    )}`;
    const res = await axios.get(tempUrl);
    setIndustries(res.data.results);
    setLoading(false);
  };

  const handleSelectChange = e => {
    setTypeValue(e.target.value);
  };

  const typeBoxes = [
    { value: "DOW", name: "Dow Jones" },
    { value: "Nasdaq100", name: "Nasdaq 100" },
    { value: "Rus. 1000", name: "Russell 1000" },
    { value: "Rus. 2000", name: "Russell 2000" },
    { value: "Rus. 3000", name: "Russell 3000" },
    { value: "SP 100", name: "SP 100" },
    { value: "SP 400", name: "SP 400 Mid Cap" },
    { value: "SP500", name: "SP 500 Large Cap" },
    { value: "SP 600", name: "SP 600 Small Cap" }
  ];

  return (
    <div>
      <Jumbotron fluid>
        <FormGroup className="d-flex align-items-center flex-column">
          <InputGroup className="mb-3" style={{ width: "50%" }}>
            <FormControl
              onChange={e => setSearchValue(e.target.value)}
              placeholder="Search Industry"
            />
            <InputGroup.Append>
              <Button onClick={handleSearchClick} variant="outline-secondary">
                Submit
              </Button>
            </InputGroup.Append>
          </InputGroup>
          <Form.Group>
            {checkboxes.map(item => (
              <Form.Check key={item.key}>
                <Checkbox
                  name={item.name}
                  checked={checkedName === item.name}
                  onChange={handleChange}
                ></Checkbox>
                {item.name}
              </Form.Check>
            ))}
          </Form.Group>

          <Form>
            <label>
              Type of Index:
              <select value={typeValue} onChange={handleSelectChange}>
                <option value="" defaultValue="" disabled hidden>
                  Choose here
                </option>
                {typeBoxes.map(type => (
                  <option key={type.value} value={type.value}>
                    {type.name}
                  </option>
                ))}
              </select>
            </label>
          </Form>
        </FormGroup>
      </Jumbotron>
      <Container className="w-100" style={{ marginBottom: "50px" }}>
        {!loading && (
          <IndustriesCards
            industries={currentIndustries}
            words={splitwords}
          ></IndustriesCards>
        )}
        {loading && <h2>loading...</h2>}
        <MyPagination
          itemsPerPage={industriesPerPage}
          totalItems={industries != null ? industries.length : 0}
          paginate={paginate}
          currentPage={currentPage}
        />
      </Container>
    </div>
  );
};

export default Industries;
