import React from "react";
import PropTypes from "prop-types";

const cstyle = {
  marginRight: "10px"
};

const Checkbox = ({ type = "radio", name, checked = false, onChange }) => (
  <input
    type={type}
    name={name}
    checked={checked}
    onChange={onChange}
    style={cstyle}
  />
);

Checkbox.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  onChange: PropTypes.func.isRequired
};

export default Checkbox;
