import React from "react";
import { Pagination } from "react-bootstrap";

const MyPagination = ({ itemsPerPage, totalItems, paginate, currentPage }) => {
  const pageNumbers = [];
  const maxPage = Math.ceil(totalItems / itemsPerPage);
  for (let number = currentPage - 3; number <= currentPage + 3; number++) {
    if (number > 0 && number <= maxPage) {
      pageNumbers.push(number);
    }
  }

  return (
    <Pagination className="justify-content-center">
      <Pagination.First onClick={() => paginate(1)}></Pagination.First>
      {pageNumbers.map(number => (
        <Pagination.Item key={number} onClick={() => paginate(number)}>
          {number}
        </Pagination.Item>
      ))}
      <Pagination.Last onClick={() => paginate(maxPage)}></Pagination.Last>
    </Pagination>
  );
};

export default MyPagination;
