import React, {Component} from "react";
import * as d3 from "d3";
import data1 from "./csv/data1.json";

export default class Visual6 extends Component {
    constructor() {
        super();
        this.state = {
            data: data1
        }
    }

    drawChart() {
        let margin = ({top: 20, right: 0, bottom: 30, left: 40});
        let width = 1200;
        let height = 800;
        let x = d3.scaleBand()
          .domain(this.state.data.map(d => d.year))
          .range([margin.left, width - margin.right])
          .padding(0.1);

        let y = d3.scaleLinear()
          .domain([0, d3.max(this.state.data, d => d.number)])
          .range([height - margin.bottom, margin.top]);

        let xAxis = g => g
          .attr("transform", `translate(0,${height - margin.bottom})`)
          .call(d3.axisBottom(x).tickSizeOuter(0));

        let yAxis = g => g
          .attr("transform", `translate(${margin.left},0)`)
          .call(d3.axisLeft(y))
          .call(g => g.select(".domain").remove());

        const svg = d3.select(this.refs.body)
          .append("svg")
          .attr('viewBox', '0 0 1200 800')
          .classed('svg-content', true);

        svg.append("g")
          .attr("fill", "steelblue")
          .selectAll("rect")
          .data(this.state.data).join("rect")
          .attr("x", d => x(d.year))
          .attr("y", d => y(d.number))
          .attr("height", d => y(0) - y(d.number))
          .attr("width", x.bandwidth());

        svg.append("g")
          .call(xAxis)
          .style('font-size', '10px');

        svg.append("g")
          .call(yAxis)
          .style('font-size', '15px');
    }
    
    componentDidMount() {
        this.drawChart();
    }

    render() {
        return (
            <React.Fragment>
                <h2 className="text-center"> Number of Energy Bills Introduced Every Year </h2>
                <div ref="body"></div>
            </React.Fragment>
        );
    }

}
