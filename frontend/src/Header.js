import React from "react";
import { Navbar, Nav } from "react-bootstrap";

const Header = () => {
  return (
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand href="/">StockOverflow</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link id="aboutHeader" href="/about">
          About
        </Nav.Link>
        <Nav.Link id="stockHeader" href="/stocks">
          Stock
        </Nav.Link>
        <Nav.Link id="industryHeader" href="/industries">
          Industry
        </Nav.Link>
        <Nav.Link id="indexHeader" href="/indices">
          Index
        </Nav.Link>
        <Nav.Link id="visualizationHeader" href="/visualization">
          Visualization
        </Nav.Link>
      </Nav>
    </Navbar>
  );
};

export default Header;
