import React, {Component} from "react";
import * as d3 from "d3";
import map_data from "./csv/map.json";

export default class Visual7 extends Component {
    constructor(pops) {
        super(pops);
        this.state = {
            data: map_data
        }
    }

    drawChart() {
        console.log(map_data);
        let width = 962,
            height = 962;
        let pack = data => d3
            .pack()
            .size([
                width - 2,
                height - 2
            ])
            .padding(3)(d3.hierarchy({children: data}).sum(d => d.value))
        let color = d3.scaleOrdinal(this.state.data.map(d => d.state), d3.schemeCategory10)
        let root = pack(this.state.data);

        let tooltip = d3
            .select("body")
            .append("div")
            .style("position", "absolute")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .style("color", "white")
            .style("padding", "8px")
            .style("background-color", "rgba(0, 0, 0, 0.75)")
            .style("border-radius", "6px")
            .style("font", "12px sans-serif")
            .text("tooltip");

        const svg = d3
            .select(this.refs.canvas)
            .append("svg")
            .attr("viewBox", [0, 0, width, height])
            .attr("font-size", 20)
            .attr("font-family", "sans-serif")
            .attr("text-anchor", "middle");

        const leaf = svg
            .selectAll("g")
            .data(root.leaves())
            .join("g")
            .attr("transform", d => `translate(${d.x + 1},${d.y + 1})`);

        leaf
            .append("circle")
            .attr("r", d => d.r)
            .attr("fill-opacity", 0.7)
            .attr("fill", d => color(d.data.state))
            .on("mouseover", function (d) {
                tooltip.text(d.data.state + ":\n" + d.value);
                tooltip.style("visibility", "visible");
            })
            .on("mousemove", function () {
                return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
            })
            .on("mouseout", function () {
                return tooltip.style("visibility", "hidden");
            });

        leaf
            .append("text")
            .selectAll("tspan")
            .data(d => d.data.state.split(/(?=[A-Z][^A-Z])/g))
            .join("tspan")
            .attr("x", 0)
            .attr("y", (d, i, nodes) => `${i - nodes.length / 2 + 0.8}em`)
            .text(d => d);
    }

    componentDidMount() {
        this.drawChart();
    }

    render() {
        return (
            <React.Fragment>
                <h2 className="text-center">
                    Number of Bills of Each State Between 1993 and 2019
                </h2>
                <div className="justify-content-center" ref="canvas"></div>
            </React.Fragment>
        )
    }
}
