import React from "react";
import { assert, expect } from "chai";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Route } from "react-router";
import ReactDOM, { Link } from "react-dom";
import App from "../src/App";
import StocksTable from "../src/components/stock/StocksTable";
import StockPage from "../src/components/stock/StockPage";
import MyPagination from "../src/components/Pagination";
import { Pagination, Form, Card, Table } from "react-bootstrap";
import About from "../src/About";
import Router from "../src/Router";
import Stocks from "../src/components/stock/Stocks";
import Header from "../src/Header";
import fetch from "isomorphic-fetch";

configure({ adapter: new Adapter() });

describe("<App />", () => {
  const wrapper = shallow(<App />);
  it("Renders without crashing", function() {
    expect(wrapper).to.not.equal(null);
  });

  it("App should have these elements", function() {
    expect(wrapper.find(Header).exists()).to.equal(false);
    expect(wrapper.find(Router).exists()).to.equal(true);
  });
});

describe("<Router />", () => {
  const wrapper = shallow(<Router />);
  const pathArray = wrapper.find(Route).reduce((pathArray, route) => {
    const routeProps = route.props();
    pathArray[routeProps.path] = routeProps.component;
    return pathArray;
  }, {});
  it("The components and the routes should be matched", function() {
    expect(pathArray["/about"]).to.not.equal(null);
    expect(pathArray["/stocks"]).to.not.equal(null);
    expect(pathArray["/industries"]).to.not.equal(null);
    expect(pathArray["/indices"]).to.not.equal(null);
  });
});

describe("<Stocks />", () => {
  const wrapper = shallow(<Stocks />);
  it("Stock should have these elements", function() {
    expect(wrapper.find(Table).exists()).to.equal(true);
    expect(wrapper.find(MyPagination).exists()).to.equal(true);
  });
  // Hook testing to be updated
});

describe("<StocksTable />", () => {
  const stock = {
    current_price: 75.77,
    indices: [{ index_ticker: "SP500", stock_ticker: "A" }],
    industry: {
      annual_movement: -0.002023316311783485,
      human_name: "Pharmaceuticals Biotechnology",
      monthly_movement: 0.005826374053214161,
      name: "pharmaceuticalsbiotechnology"
    },
    industry_name: "pharmaceuticalsbiotechnology",
    name: "AGILENT TECHNOLOGIES INC",
    ticker: "A",
    volume: 66833,
    website: null
  };
  const wrapper = shallow(<StocksTable stocks={[stock]} loading="true" />);
  it("StockTable shoud have <tbody> and <tr> tag", function() {
    expect(wrapper.find("tbody")).to.have.lengthOf(1);
    expect(wrapper.find("tr")).to.have.lengthOf(2);
  });
  it("includes link to a cutom page based on the info", () => {
    expect(wrapper.find("Link")).to.have.lengthOf(1);
    expect(wrapper.find("Link").props().to).to.equal("/stocks/A");
  });
});

describe("<Pagination />", () => {
  const wrapper = shallow(<MyPagination itemsPerPage={4} totalItems={40} />);
  it("Pagination page has Pagination tags", function() {
    expect(wrapper.find(Pagination).length).to.equal(1);
  });
  it("The loop inside Pagination should loop exactly number of times designated", function() {
    expect(wrapper.find(Pagination.Item).length).to.equal(0);
  });
});

describe("<About />", () => {
  const wrapper = shallow(<About />);
  it("There are six cards for creator profile", function() {
    expect(wrapper.find(Card.Body).length).to.equal(14);
  });
});

describe("<StockPage />", () => {
  const match = {
    params: {
      stock: "A"
    }
  };
  const wrapper = shallow(<StockPage match={match} />);
  it("There is a list in the page", function() {
    expect(
      wrapper
        .find("ul")
        .find("li")
        .exists()
    ).to.equal(false);
  });
  it("There are correct number of <li>", function() {
    expect(wrapper.find("li").length).to.equal(0);
  });
});
