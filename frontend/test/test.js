var assert = require("assert");
var webdriver = require("selenium-webdriver");
require("geckodriver");
var chrome = require("selenium-webdriver/chrome");
var path = require("chromedriver").path;
const serverUri = "http://localhost:3000";
var service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);

var browser = new webdriver.Builder()
  .withCapabilities(webdriver.Capabilities.chrome())
  .build();

/**
 * Config for Chrome browser
 * @type {webdriver}
 */

function getTitle() {
  return new Promise((resolve, reject) => {
    browser.getTitle().then(function(title) {
      resolve(title);
    });
  });
}

describe("Test #1", function() {
  it("Should check Stockoverflow is the title of our website", function() {
    browser.get(serverUri);
    browser.then(function() {
      return new Promise((resolve, reject) => {
        browser
          .then(getTitle)
          .then(title => {
            assert.equal(title, "Stockoverflow");
            resolve();
          })
          .catch(err => reject(err));
      });
    });
  });
});

describe("Test #2", function() {
  it("Should check whether stockButton is clickable and correctly linking to a stocktable page", function() {
    browser.get(serverUri);
    browser
      .findElement({ id: "stockButton" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(currentUrl, "https://www.stockoverflow.me/stocks");
          resolve().catch(err => reject(err));
        });
      });
  });
});

describe("Test #3", function() {
  it("Should check whether industryButton is clickable and correctly linking to a industrytable page", function() {
    browser.get(serverUri);
    browser
      .findElement({ id: "industryButton" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(currentUrl, "https://www.stockoverflow.me/industries");
          resolve().catch(err => reject(err));
        });
      });
  });
});

describe("Test #4", function() {
  it("Should check whether indexButton is clickable and correctly linking to a indextable page", function() {
    browser.get(serverUri);
    browser
      .findElement({ id: "indexButton" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(currentUrl, "https://www.stockoverflow.me/indices");
          resolve().catch(err => reject(err));
        });
      });
  });
});

describe("Test #5", function() {
  it("Should check whether About on the header is clickable and correctly linking to our about page", function() {
    browser.get(serverUri);
    browser
      .findElement({ id: "aboutHeader" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(currentUrl, "https://www.stockoverflow.me/About");
          resolve().catch(err => reject(err));
        });
      });
  });
});

describe("Test #6", function() {
  it("Should check whether Stock on the header is clickable and correctly linking to our stocks page", function() {
    browser.get(serverUri);
    browser
      .findElement({ id: "stockHeader" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(currentUrl, "https://www.stockoverflow.me/stocks");
          resolve().catch(err => reject(err));
        });
      });
  });
});

describe("Test #7", function() {
  it("Should check whether Industry on the header is clickable and correctly linking to our industries page", function() {
    browser.get(serverUri);
    browser
      .findElement({ id: "industryHeader" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(currentUrl, "https://www.stockoverflow.me/industries");
          resolve().catch(err => reject(err));
        });
      });
  });
});

describe("Test #8", function() {
  it("Should check whether Index on the header is clickable and correctly linking to our indices page", function() {
    browser.get(serverUri);
    browser
      .findElement({ id: "indexHeader" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(currentUrl, "https://www.stockoverflow.me/indices");
          resolve().catch(err => reject(err));
        });
      });
  });
});

describe("Test #9", function() {
  it("Should check whether postman on About page is correctly linking to our postman document page", function() {
    browser.get(serverUri);
    browser.findElement({ id: "aboutHeader" }).click();
    browser
      .findElement({ id: "postman" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(
            currentUrl,
            "https://gitlab.com/https://documenter.getpostman.com/view/726128/SVtPYquQ?version=latest/stock-overflow"
          );
          resolve().catch(err => reject(err));
        });
      });
  });
});

describe("Test #10", function() {
  it("Should check whether gitrepo on About page is correctly linking to our gitlab repository", function() {
    browser.get(serverUri);
    browser.findElement({ id: "aboutHeader" }).click();
    browser
      .findElement({ id: "gitrepo" })
      .click()
      .then(function() {
        return new Promise((resolve, reject) => {
          const currentUrl = browser.getCurrentUrl();
          assert.equal(
            currentUrl,
            "https://documenter.getpostman.com/view/726128/SVtPYquQ?version=latest"
          );
          resolve().catch(err => reject(err));
        });
      });
  });
});

after(function() {
  browser.quit();
});
