import unittest
import requests
import json

# python -m unittest test_app


class TestMyApp(unittest.TestCase):
    def test1 (self):
        response = requests.get("https://www.stockoverflow.me/api/index")
        assert response.status_code == 200
    def test2 (self):
        response = requests.get("https://www.stockoverflow.me/api/stock")
        assert response.status_code == 200
    def test3 (self):
        response = requests.get("https://www.stockoverflow.me/api/industry")
        assert response.status_code == 200
    def test4 (self):
        response = requests.get("https://www.stockoverflow.me/api/index/DOW")
        assert response.status_code == 200
    def test5 (self):
        response = requests.get("https://www.stockoverflow.me/api/stock/A")
        assert response.status_code == 200
    def test6 (self):
        response = requests.get("https://www.stockoverflow.me/api/industry/foodstaplesretailing")
        assert response.status_code == 200
    def test7 (self):
        response = requests.get("https://www.stockoverflow.me/api/industry/foodstaplesretailing")
        assert response.json()["human_name"] == "Food Staples Retail"
    def test8 (self):
        response = requests.get("https://www.stockoverflow.me/api/industry/192740")
        assert response.json() == {}
    def test9 (self):
        response = requests.get("https://www.stockoverflow.me/api/stock/A")
        assert response.json()["name"] == "AGILENT TECHNOLOGIES INC"
    def test10 (self):
        response = requests.get("https://www.stockoverflow.me/api/stock/12u01u501")
        assert response.json() == {}
    def test11 (self):
        response = requests.get("https://www.stockoverflow.me/api/index/DOW")
        assert response.json()["ticker"] == "DOW"
    def test12 (self):
        response = requests.get("https://www.stockoverflow.me/api/index/12u01u501")
        assert response.json() == {}

if __name__== '__main__':
    unittest.main()
