FROM nikolaik/python-nodejs:latest

ADD frontend /myapp/src

ADD backend /myapp/src

WORKDIR /myapp/src

RUN yarn install

RUN yarn build

RUN rm -rf /myapp/src/node_modules

RUN rm -rf /myapp/src/public

RUN rm -rf /myapp/src/src

ENV PYTHONUNBUFFERED 1

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

RUN find /myapp/src/

CMD ["python", "main.py"]
