# StockOverflow

Displaying stock information with React JS

Website: https://www.stockoverflow.me
Gitlab Pipeline: https://gitlab.com/adarsha100/stock-overflow/pipelines
Git SHA: 28007fee07e9694a3bfb8f4ca11140ebda6608da

Project Leaders:
Phase 1: Ick Namgung
Phase 2: Simon Xie
Phase 3: Andrew Chang
Phase 4: Alan Ding

Andrew Chang
EID: amc7994
Gitlab: @chang.andrew
Expected: 40hrs
Actual: 50hrs

Zhimin Ding
EID: zd2679
Gitlab: @dingz9926
Expected: 30hrs
Actual: 35hrs

Adarsha Regmi
EID: Ar58366
Gitlab: @adarsha100
Expected: 30hrs
Actual: 35hrs

Jianjian Xie
EID:jx3422
Gitlab: @yangwenli1
Expected: 35hrs
Actual: 45hrs

Ick Namgung
EID: in989
Gitlab: @ik0675
Expected: 40hrs
Actual 50hrs
