import pickle
from pprint import pprint

sectors = pickle.load( open( "sectors_dict.pkl", "rb" ) )
sec = [sector for sector in sectors]
#pprint(sec)

sec.sort(key=lambda x : len(sectors[x]))

# pprint(sec)


sector_to_ETF = {
 'foodstaplesretailing'           :    'RHS',
 'householdpersonalproducts'      :    'XLY',
 'telecommunicationservices'      :    'XTL',
 'utilities'                      :    'XLU',
 'technologyhardwareequipment'    :    'XLK',
 'diversifiedfinancials'          :    'XLF',
 'materials'                      :    'XLB',
 'energy'                         :    'XLE', 
 'pharmaceuticalsbiotechnology'   :    'IBB'
}

new_name_to_old = {
 'foodstaplesretailing'          :      'consumer staples',
 'householdpersonalproducts'     :      'consumer discretionary',
 'telecommunicationservices'     :      'communication services',
 'utilities'                     :      'utilities',
 'technologyhardwareequipment'   :      'technology', 
 'diversifiedfinancials'         :      'financial',
 'materials'                     :      'materials',
 'energy'                        :      'energy',
 'pharmaceuticalsbiotechnology'  :       'healthcare'
}

sectors_trim = {name:sectors[name] for name in sector_to_ETF}

pprint(sectors_trim)