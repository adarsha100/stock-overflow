from pprint import pprint
from collections import defaultdict
import pickle
import json
import requests


"""
builds the list of all supported tickers 
and creates a dict of each stack and its base data
data for an individual ticker looks like:

we use lastSalePrice as the current price

{
  "symbol": "AAPL",
  "sector": "technologyhardwareequipment",
  "securityType": "commonstock",
  "bidPrice": 0,
  "bidSize": 0,
  "askPrice": 0,
  "askSize": 0,
  "lastUpdated": 1571169688499,
  "lastSalePrice": 235.37,
  "lastSaleSize": 100,
  "lastSaleTime": 1571169602924,
  "volume": 361170,
  "marketPercent": 0.01669
}

^^ how the api returns it


{ 
    "AAPL" : 
    {
        "symbol": "AAPL",
        "sector": "technologyhardwareequipment",
        "securityType": "commonstock",
        "bidPrice": 0,
        "bidSize": 0,
        "askPrice": 0,
        "askSize": 0,
        "lastUpdated": 1571169688499,
        "lastSalePrice": 235.37,
        "lastSaleSize": 100,
        "lastSaleTime": 1571169602924,
        "volume": 361170,
        "marketPercent": 0.01669
    }

}

^^ what we build

also make a dict of sectornames and components
and set of sectornames
"""


bulk_url = "https://api.iextrading.com/1.0/tops"
r = requests.get(url=bulk_url)
all_stock_base_data = r.json()
all_tickers = set()
sectors = defaultdict(list)

all_stocks = defaultdict(dict)

q = requests.get(url="https://api.iextrading.com/1.0/ref-data/symbols")
sym = q.json()


for d in all_stock_base_data: # a list of dicts
    ticker = d['symbol']
    sector = d['sector']
    sectors[sector].append(ticker)
    all_tickers.add(ticker)
    all_stocks[ticker] = d

for d in sym:
    ticker = d['symbol']
    all_stocks[ticker]['name'] = d['name']





# with open('allticksers_list.pkl', 'wb') as f:
#     pickle.dump(all_tickers, f, pickle.HIGHEST_PROTOCOL)

# with open('basestockdata_dict.pkl', 'wb') as f:
#     pickle.dump(all_stocks, f, pickle.HIGHEST_PROTOCOL)

# with open('sectors_dict.pkl', 'wb') as f:
#     pickle.dump(sectors, f, pickle.HIGHEST_PROTOCOL)


    


