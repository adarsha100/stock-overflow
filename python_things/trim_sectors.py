from pprint import pprint
from collections import defaultdict
import json as Json
import pickle
import requests

sectors = pickle.load( open( "sectors_dict.pkl", "rb" ) )
sec = [sector for sector in sectors]
#pprint(sec)

sec.sort(key=lambda x : len(sectors[x]))

# pprint(sec)


sector_to_ETF = {
 'foodstaplesretailing'           :    'RHS',
 'householdpersonalproducts'      :    'XLY',
 'telecommunicationservices'      :    'XTL',
 'utilities'                      :    'XLU',
 'technologyhardwareequipment'    :    'XLK',
 'diversifiedfinancials'          :    'XLF',
 'materials'                      :    'XLB',
 'energy'                         :    'XLE', 
 'pharmaceuticalsbiotechnology'   :    'IBB'
}

new_name_to_old = {
 'foodstaplesretailing'          :      'consumer staples',
 'householdpersonalproducts'     :      'consumer discretionary',
 'telecommunicationservices'     :      'communication services',
 'utilities'                     :      'utilities',
 'technologyhardwareequipment'   :      'technology', 
 'diversifiedfinancials'         :      'financial',
 'materials'                     :      'materials',
 'energy'                        :      'energy',
 'pharmaceuticalsbiotechnology'  :       'healthcare'
}

sectors_trim = {name:sectors[name] for name in sector_to_ETF}
newdict = defaultdict(dict)

for sector in sectors_trim:
    stocks = sectors_trim[sector]
    newdict[sector]['stocks'] = stocks
    newdict[sector]['annual_change'] = float()
    newdict[sector]['monthly_change'] = float()

sectors_trim = newdict

# a sector_trim looks like

# 'TICKER':
#     stocks : []
#     monthly_change = n
#     annual_change = n


# q = requests.get(url="https://api.iextrading.com/1.0/ref-data/symbols")
# sym = q.json()

URL =  "https://api.unibit.ai/api/historicalstockprice/{}?range={}&interval={}&AccessKey=KMSUv1VaR0x86h_yFzr-LaYHMfxWXRBc"
# sample response

for sector in sectors_trim:
    ETF = sector_to_ETF[sector]
    q = requests.get(URL.format(ETF, '1m', '1'))
    json = q.json()
    mopenp = float(json['Stock price'][-1]['close'])
    mclosep = float(json['Stock price'][0]['close'])
    monthly_change = (mclosep - mopenp) / mopenp
    sectors_trim[sector]['monthly_change'] = monthly_change

    q = requests.get(URL.format(ETF, '1y', '1'))
    json = q.json()
    yopenp = float(json['Stock price'][-1]['close'])
    yclosep = float(json['Stock price'][0]['close'])
    annual_change = (yclosep - yopenp) / yopenp
    sectors_trim[sector]['annual_change'] = annual_change
    
    # for some reason the annual and monthly are the same


# pprint(sectors_trim)
pprint(Json.dumps(sectors_trim))


with open('sectors_trim_dict_final.pkl', 'wb') as handle:
    pickle.dump(sectors_trim, handle, protocol=pickle.HIGHEST_PROTOCOL)


# {
#   "Meta Data" : {
#     "ticker" : "AAPL",
#     "datapoints" : 1,
#     "interval" : "30 day(s)",
#     "credit cost" : 10,
#     "Time Zone" : "America/New_York",
#     "last refreshed" : "10/21/2019 at 11:22PM ET"
#   },
#   "Stock price" : [ {
#     "date" : "2019-10-21",
#     "open" : 237.52,
#     "high" : 240.99,
#     "low" : 237.48,
#     "close" : 240.51,
#     "adj_close" : 240.51,
#     "volume" : 21469687
#   } ]
# }



# pprint(sectors_trim)