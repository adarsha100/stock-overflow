import pickle
import json 
import requests
from pprint import pprint

index_to_ticker = {
    'SP100': 'OEF', 
    'SP400 Mid Cap' : 'MDY',
    'SP600 Small Cap': 'SLY',
    'Russell 3000' : 'IWV',
    'Russell 2000' : 'VTWO',
    'Russell 1000' : 'VONE'
}   

# index = {
#     'DOW' : {
#         'components' : [],
#         'monthly_change' : float(),
#         'annual_change' : float()
#     },
#     'SP500' : {
#         'components' : [],
#         'monthly_change' : float(),
#         'annual_change' : float()
#     },
#     'Nasdaq100' : {
#         'components' : [],
#         'monthly_change' : float(),
#         'annual_change' : float()
#     }
# }


# Russell 1k, 2k, 3k SP mid cap 400, 100, SP small cap 600


URL =  "https://api.unibit.ai/api/historicalstockprice/{}?range={}&interval={}&AccessKey=KMSUv1VaR0x86h_yFzr-LaYHMfxWXRBc"
stocks = pickle.load( open( "basestockdata_dict.pkl", "rb" ) ) # dict of dicts
index = pickle.load( open( "indexdata_dict.pkl", "rb" ) ) # dict of dicts, 'index' : components[], monthly: 0 annual: 0


def add_index(index_name, ticker_list):
    for ticker in ticker_list:
        if ticker in stocks:    # only add the ticker if we support it
            if index_name not in index:
                index[index_name] = dict()
                index[index_name]['components'] = []
            if ticker not in index[index_name]['components']:
                index[index_name]['components'].append(ticker)
            if stocks[ticker].get('index') is not None and index_name not in stocks[ticker]['index']:   # don't add a ticker twice
                stocks[ticker]['index'].append(index_name)
    

def update_change_all(index_list): # adds price information to each index
    for idx in index_list:
        ticker = index_to_ticker[idx]
        q = requests.get(URL.format(ticker, '1m', '1'))
        json = q.json()
        mopenp = float(json['Stock price'][-1]['close'])
        mclosep = float(json['Stock price'][0]['close'])
        monthly_change = (mclosep - mopenp) / mopenp
        index[idx]['monthly_change'] = monthly_change

        q = requests.get(URL.format(ticker, '1y', '1'))
        json = q.json()
        yopenp = float(json['Stock price'][-1]['close'])
        yclosep = float(json['Stock price'][0]['close'])
        annual_change = (yclosep - yopenp) / yopenp
        index[idx]['annual_change'] = annual_change




sp100 = [line.strip().split()[0] for line in open('sp100.txt', 'r')]
sp400mid = [line.strip().split(',')[0] for line in open('sp400.csv', 'r')]
sp600 = [line.strip().split()[0] for line in open('sp600.txt', 'r')]
russell1k = [line.strip().split()[-1] for line in open('r1k.txt', 'r')]
russell2k = [line.strip().split()[0] for line in open('r2k.txt','r')]
russell3k = [line.strip().split()[-1] for line in open('r3k.txt', 'r')]

ind = {
    'SP100' : sp100,
    'SP400 Mid Cap' : sp400mid,
    'SP600 Small Cap' : sp600,
    'Russell 3000' : russell3k,
    'Russell 2000' : russell2k, 
    'Russell 1000' : russell1k
}

for name, index_list in ind.items():
    add_index(name, index_list)

update_change_all(ind)

with open('nineIndex.pkl', 'wb') as f:
    pickle.dump(index, f, pickle.HIGHEST_PROTOCOL)

with open('newStocks.pkl', 'wb') as f:
    pickle.dump(stocks, f, pickle.HIGHEST_PROTOCOL)
