from pprint import pprint
import json as Json
import requests
import pickle 


# an index object looks like 

index_to_ticker = {
    'DOW' : 'DIA',
    'SP500' : 'SPY',
    'Nasdaq100' : 'QQQ'
}

index = {
    'DOW' : {
        'components' : [],
        'monthly_change' : float(),
        'annual_change' : float()
    },
    'SP500' : {
        'components' : [],
        'monthly_change' : float(),
        'annual_change' : float()
    },
    'Nasdaq100' : {
        'components' : [],
        'monthly_change' : float(),
        'annual_change' : float()
    }
}

spy = []
with open('sp.txt', 'r') as f:
    for line in f:
        stop = line.index(',')
        spy.append(line[0:stop])

stocks = pickle.load( open( "basestockdata_dict.pkl", "rb" ) ) # dict of dicts
dow = [sym.strip() for sym in open('dow.txt', 'r')]
assert len(dow) == 30
nasdaq = [line.split()[-1] for line in open('ndq.txt', 'r')]

for ticker in dow:
    index['DOW']['components'].append(ticker)

for ticker in spy:
    index['SP500']['components'].append(ticker)

for ticker in nasdaq:
    index['Nasdaq100']['components'].append(ticker)

URL =  "https://api.unibit.ai/api/historicalstockprice/{}?range={}&interval={}&AccessKey=KMSUv1VaR0x86h_yFzr-LaYHMfxWXRBc"
# sample response

for idx in index:
    ticker = index_to_ticker[idx]
    q = requests.get(URL.format(ticker, '1m', '1'))
    json = q.json()
    mopenp = float(json['Stock price'][-1]['close'])
    mclosep = float(json['Stock price'][0]['close'])
    monthly_change = (mclosep - mopenp) / mopenp
    index[idx]['monthly_change'] = monthly_change

    q = requests.get(URL.format(ticker, '1y', '1'))
    json = q.json()
    yopenp = float(json['Stock price'][-1]['close'])
    yclosep = float(json['Stock price'][0]['close'])
    annual_change = (yclosep - yopenp) / yopenp
    index[idx]['annual_change'] = annual_change
    
    # for some reason the annual and monthly are the same

with open('indexdata_dict.pkl', 'wb') as f:
    pickle.dump(index, f, pickle.HIGHEST_PROTOCOL)

pprint(Json.dumps(index))