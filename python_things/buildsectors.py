from collections import defaultdict
import pickle

"""
builds sectordata
each sector contains a list of component companies
along with a change in price over a certain interval
"""

def load_obj(filename):
    with open(filename, 'rb') as f:
        return pickle.load(f)

all_tickers = load_obj('alltickers_list.pkl')   # a list of tickers
all_stocks = load_obj('allstockdata_json.pkl')  # a list of dicts

sector_data = defaultdict(list)

