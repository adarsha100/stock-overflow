import pickle
from pprint import pprint
import json

stocks = pickle.load( open( "newStocks.pkl", "rb" ) ) 
index = pickle.load( open( "nineIndex.pkl", "rb" ) )

with open('stocks.json', 'w') as fp:
    json.dump(stocks, fp)

with open('index.json', 'w') as fp:
    json.dump(index, fp)

