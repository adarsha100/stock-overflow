#!/usr/bin/env python
# coding: utf-8

# In[1]:


import requests
import json


# In[2]:


response = requests.get("https://www.localhost.me/api/stock")


# In[3]:


stock_json = json.loads(response.text)


# In[17]:


price_0_stock_name = []
stocks = stock_json['objects']
for stock in stocks:
    if stock['current_price'] == 0:
        price_0_stock_name.append(stock['ticker'])


# In[16]:


# stock_json['objects'][0]


# In[39]:


# len(price_0_stock_name)


# In[37]:


# price_0_stock_name


# In[28]:


for name in price_0_stock_name:
    delete_url = "http://0.0.0.0:5000/api/stock/" + name
#     print(delete_url)
    requests.delete(delete_url)


# In[ ]:




