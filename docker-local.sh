# Must be in same directory as Dockerfile

#build new docker image version
sudo docker image build -t stockoverflow .

#run docker container locally
sudo docker container run --publish 5000:5000 --detach --name stockoverflow stockoverflow:latest
